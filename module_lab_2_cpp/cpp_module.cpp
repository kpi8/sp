/*
 * C++ component inside the Linux kernel
 */
#include <linux/kernel.h>
#include <linux/module.h>

#include "cpp_module.h"
#include "logger.h"

#define NULL 0

class Processor {
public:
    Processor()
        :a(0)
    {
        kern_log("C++ class constructor\n");
    }

    ~Processor()
    {
        kern_log("C++ class destructor\n");
    }

    void set_data(int a)
    {
        kern_log("setting data %d to a parameter 'a'\n", a);
        this->a = a;
    }
    int get_data()
    {
        kern_log("getting data %d from a parameter 'a'\n", a);
        return this->a;
    }

private:
    int a;
};

static Processor *test_obj = NULL;

/* This functions can be called from the C code */
void init_cpp_subsystem(void)
{
    test_obj = new Processor;

    if (!test_obj) {
        kern_log("Failed to allocate bar class\n");
        return;
    }

    test_obj->set_data(12);

    test_obj->get_data();
}

void exit_cpp_subsystem(void)
{    
    if (test_obj) {
        delete test_obj;
    }
}