#pragma once
#include <iostream>
#include <string>
#include <set>

using namespace std;

enum FamilyState
{
	Free,
	Married,
	EverythingComplicated,
};

enum BadHabits
{
	Alcohol,
	Smoking,
};

enum Sex
{
	Male,
	Female,
};

class Customer {

public:
	Customer(int customerCode, string pib, Sex sex, string birthDay, unsigned age, double height, double weight,
		int childrenCount, FamilyState familyState, set<BadHabits> badHabits, string hobby, string description,
		int signCode, int relationsCode, int nationalityCode, string address, string phone, string passportData,
		string partnerInfo);

	~Customer();
	void print_pib();
	void print_public_info();
	void print_private_info();
	void print_other_info();


private:
	string pib_;
	Sex sex_;
	unsigned age_;
	double height_;
	double weight_;
	set<BadHabits> bad_habits_;
	string hobby_;
	string description_;

	string address_;
	string phone_;

	string birth_day_;
	FamilyState family_state_;
	int children_count_;
	int nationality_code_;
	string passport_data_;

	int customer_code_;
	int sign_code_;
	int relations_code_;
	string partner_info_;

	void print_appearance();
	void print_leisure();
	void print_contact_info();
	void print_passport_info();

	string get_sex();
};

