#include "Customer.h"

int main() {
	Customer sc1 = Customer(1, "Stepan Stepanyk Stepankovich 0", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	sc1.print_pib();
	sc1.print_public_info();
	sc1.print_private_info();
	Customer sc2 = Customer(2, "Stepan Stepanyk Stepankovich 1", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	sc2.print_pib();
	sc2.print_public_info();
	sc2.print_other_info();
	Customer sc3 = Customer(3, "Stepan Stepanyk Stepankovich 2", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	sc3.print_pib();
	sc3.print_private_info();
	sc3.print_other_info();
	Customer sc4 = Customer(4, "Stepan Stepanyk Stepankovich 3", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	sc4.print_pib();
	sc4.print_private_info();
	sc4.print_other_info();
	Customer sc5 = Customer(5, "Stepan Stepanyk Stepankovich 4", Female, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking,Alcohol }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	sc5.print_pib();
	sc5.print_private_info();

	printf("\n");
	Customer* hc1 = new Customer(6, "Stepan Stepanyk Stepankovich 5", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	hc1->print_pib();
	hc1->print_private_info();
	delete hc1;
	Customer* hc2 = new Customer(7, "Stepan Stepanyk Stepankovich 6", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	hc2->print_pib();
	hc2->print_private_info();
	delete hc2;
	Customer* hc3 = new Customer(8, "Stepan Stepanyk Stepankovich 7", Female, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	hc3->print_pib();
	hc3->print_private_info();
	delete hc3;
	Customer* hc4 = new Customer(9, "Stepan Stepanyk Stepankovich 8", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	hc4->print_pib();
	hc4->print_private_info();
	delete hc4;
	Customer* hc5 = new Customer(10, "Stepan Stepanyk Stepankovich 9", Male, "21.01.2000", 21, 180, 70, 0, EverythingComplicated, { Smoking,Alcohol }, "None", "None", 2, 1, 23, "Kherson", "+3442374234", "None", "None");
	hc5->print_pib();
	hc5->print_private_info();
	hc5->print_public_info();
	hc5->print_other_info();
	delete hc5;

}
