
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

#define MACH_NAME "mouse" //Matched name
 #define EVENT_X_FORMAT "/dev/input/event%d" //Device file path in the dev directory
int test_mouse(char *mousedev);
 //Find the subscript of the input device according to the device name
int get_event_x(char *mach_name);


int main(void)
{
    int ret = 0;
    int x   = 0;
    char file_path_buffer[100]= {0};

    x = get_event_x(MACH_NAME);
    if(x<0) {
                 printf("%s does not exist!\n", MACH_NAME);
        exit(0);
    }
    sprintf(file_path_buffer,EVENT_X_FORMAT,x);
    ret = test_mouse(file_path_buffer);

    return ret;
}



int init_device(char *dev)
{
    file_permission(dev);
    int fd;
    if((fd = open(dev, O_RDONLY)) < 0) {
        printf("Error open %s\n\n", dev);
        return -1;
    }

    return fd;
}

int test_mouse(char *mousedev)
{
    static struct input_event data;
    int fd;
    fd=init_device(mousedev);
    if( fd < 0) {
        return -1;
    }

    while(1) {
        read(fd, &data, sizeof(data));
        if (data.type == EV_KEY) {
            printf("type = EV_KEY, code = %s, value = %d\n",
                   data.code == BTN_LEFT ? "MOUSE_LEFT" :
                   data.code == BTN_RIGHT ? "MOUSE_RIGHT" :
                   data.code == BTN_MIDDLE ? "MOUSE_MIDDLE" :
                   data.code == BTN_SIDE ? "MOUSE_SIDE" :
                   "Unkonw", data.value);

        } else if(data.type == EV_REL) {
            printf("type = EV_REL, code = %s, value = %d\n",
                   data.code == REL_X ? "REL_X" :
                   data.code == REL_Y ? "REL_Y" :
                   data.code == ABS_WHEEL ? "MOUSE_WHEEL" :
                   data.code == ABS_PRESSURE ? "ABS_PRESSURE" :
                   "Unkown", data.value);

        }
    }
    return 0;
}


 //Determine whether the file is saved
int file_is_exist(char *filepath)
{
    int ret;
    if((access(filepath,F_OK))!=-1) {
                 printf("File %s exists.\n",filepath);
        ret = 0;
    } else {
                 printf("%s does not exist!\n", filepath);
        ret = -1;
    }
    return ret;
}


 //Find the subscript of the input device according to the device name
int get_event_x(char *mach_name)
{
 //Search path
#define EVENT_FILE_FORMAT  "/sys/class/input/event%d/device/name"
    int i= 0;
    int j;
    size_t len = 0;
    ssize_t read;
    FILE * flip;
    char *line;
    char file_path_buffer[100]= {0};
    char file_data_buffer[100]= {0};
    char tem_mach_name[100]= {0};
    for(i=0; i<32; i++) {
        sprintf(file_path_buffer,EVENT_FILE_FORMAT,i);
        if(!file_is_exist(file_path_buffer)) {
            flip = fopen(file_path_buffer, "r");
            if(flip==NULL) {
                printf("cant open the file\n");
                continue;
            }

            //fread(file_data_buffer,100,1,flip);
           read = getline(&line, &len, flip);
            if(read == -1)
            {
                printf("read the file error\n");
                return -1;
            }

            stpncpy(file_data_buffer,line,read );
            if (line)
                free(line);

            printf("%s\n",file_data_buffer);

            j = 0;
            while(file_data_buffer[j++]) {
                file_data_buffer[j] = tolower(file_data_buffer[j]);
            }

            j = 0;
            while(tem_mach_name[j++]) {
                tem_mach_name[j] = tolower(mach_name[j]);
            }

            if(strstr(file_data_buffer,mach_name)) {
                return i;
            }
        }
    }

    return -1;
}

int file_permission(char *filepath)
{
    if((access(filepath,F_OK))!=-1) {
                 printf("File %s exists.\n",filepath);
    } else {
                 printf("%s does not exist!\n", filepath);
    }

    if(access(filepath,R_OK)!=-1) {
                 printf("%s has read permission\n", filepath);
    } else {
                 printf("%s is unreadable.\n", filepath);
    }

    if(access(filepath,W_OK)!=-1) {
                 printf("%s has write permission\n", filepath);
    } else {
                 printf("%s is not writable.\n",filepath);
    }
    if(access(filepath,X_OK)!=-1) {
                 printf("%s has executable permissions\n", filepath);
    } else {
                 printf("%s is not executable.\n",filepath);
    }
}
