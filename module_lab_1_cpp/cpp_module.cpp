/*
 * C++ component inside the Linux kernel
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include "cpp_module.h"
#include "logger.h"

#define NULL 0

void init_cpp_subsystem(void)
{
    kern_log("Hello, kernel!\n");
}

void exit_cpp_subsystem(void)
{
    kern_log("Bye, kernel!\n");
}