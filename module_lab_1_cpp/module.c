/*
 * Linux kernel module 
 */

#include <linux/kernel.h>
#include <linux/module.h>

#include "logger.h"
#include "cpp_module.h"

static int __init module_load(void)
{
    kern_log("Loading C++ kernel module\n");

    init_cpp_subsystem();

    return 0;
}

static void __exit module_unload(void)
{
    kern_log("Unloading C++ kernel module\n");

    exit_cpp_subsystem();
}

module_init(module_load);
module_exit(module_unload);

MODULE_SUPPORTED_DEVICE ("cpp_kernel");
MODULE_AUTHOR("(Kusik, Grygorenko)");  // Подарунок
MODULE_DESCRIPTION("Lab1 test kernel module"); // Опис фунціоналу
MODULE_LICENSE("MIT"); 