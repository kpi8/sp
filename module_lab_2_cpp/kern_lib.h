/*
 * Kernel lib - support basic C++ runtime functions
 */

#ifndef KERN_LIB_H
#define KERN_LIB_H

#ifdef __cplusplus
#include <cstdarg>
extern "C" {
#else
#include <stdarg.h>
#endif

void *kcmalloc(unsigned int size);
void *kcrealloc(void *mem, unsigned int size);
void kcfree(void *mem);

#ifdef __cplusplus
}
#endif

#endif