/*
 * Kernel lib - support basic C++ runtime functions
 */

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/string.h>

#include "kern_lib.h"

void *kcmalloc(unsigned int size)
{
    return kmalloc(size, GFP_ATOMIC);
}

void *kcrealloc(void *mem, unsigned int size)
{
    return krealloc(mem, size, GFP_ATOMIC);
}

void kcfree(void *mem)
{
    kfree(mem);
}