#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QTextEdit>
#include <string>
#include <vector>
#include <QTableView>
#include <QStandardItemModel>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
 void handleButton();

private:
 QLineEdit *inputField;
 QTableView *outputField;
 QVector<QPair<QString,qlonglong>> GetFilesInfo(QString path);
 QStandardItemModel *model;

};
#endif // MAINWINDOW_H
