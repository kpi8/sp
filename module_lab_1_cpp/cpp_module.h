/*
 */

#ifndef CPP_MODULE_H
#define CPP_MODULE_H

#ifdef __cplusplus
extern "C" {
#endif
void init_cpp_subsystem(void);
void exit_cpp_subsystem(void);
#ifdef __cplusplus
}
#endif

#endif