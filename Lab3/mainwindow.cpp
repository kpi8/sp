#include "mainwindow.h"
#include <QPushButton>
#include <QTableView>
#include <QString>
#include <QDir>
#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
     setWindowTitle("Lab3");
     setGeometry(100, 100, 650, 500);
     setMinimumSize(200, 200);

     outputField = new QTableView(this);
     outputField->setGeometry(10, 10, 600, 400);
     //outputField->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );

     inputField = new QLineEdit(this);
     inputField->setGeometry(10, 450, 400, 20);

     QPushButton *okButton = new QPushButton("Show", this);
     okButton->setGeometry(455, 450, 50, 20);
     connect(okButton, SIGNAL (released()), this, SLOT (handleButton()));
}

void MainWindow::handleButton()
{
    QVector<QPair<QString,qlonglong>> data = GetFilesInfo((inputField->text()));
    int rowCount = data.size();
    model = new QStandardItemModel(rowCount,2,this);

    // Attach the model to the view
    outputField->setModel(model);
    outputField->setColumnWidth(0,400);
    outputField->setColumnWidth(1,180);
    // Generate data
    for(int row = 0; row < rowCount; row++)
    {
        {
            QModelIndex index= model->index(row,0,QModelIndex());
            model->setData(index,data[row].first);
        }
        {
            QModelIndex index= model->index(row,1,QModelIndex());
            model->setData(index, QString::number(data[row].second) + " byte");
        }
    }

    //inputField->clear();
}

QVector<QPair<QString,qlonglong>> MainWindow::GetFilesInfo(QString path)
{
    QVector<QPair<QString,qlonglong>> data;

    QDir directory(path);
    QStringList files = directory.entryList(QDir::Files);

    foreach(QString filename, files)
    {
        QFile file(directory.filePath(filename));
//        file.open(QIODevice::ReadOnly);
        data.push_back(qMakePair(filename,qlonglong(file.size())));
//        file.close();
    }

    return data;
}

MainWindow::~MainWindow()
{
}

