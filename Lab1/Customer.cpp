#include <string>
#include "Customer.h"

using namespace std;

Customer::Customer(int customerCode, string pib, Sex sex, string birthDay, unsigned age, double height,
	double weight, int childrenCount, FamilyState familyState, set<BadHabits> badHabits, string hobby,
	string description, int signCode, int relationsCode, int nationalityCode, string address, string phone,
	string passportData, string partnerInfo) : pib_(std::move(pib)), sex_(sex), age_(age),
	height_(height), weight_(weight), bad_habits_(std::move(badHabits)), hobby_(std::move(hobby)),
	description_(std::move(description)), address_(std::move(address)),
	phone_(std::move(phone)), birth_day_(birthDay),
	family_state_(familyState), children_count_(childrenCount),
	nationality_code_(nationalityCode), passport_data_(std::move(passportData)),
	customer_code_(std::move(customerCode)), sign_code_(signCode),
	relations_code_(relationsCode),
	partner_info_(std::move(partnerInfo))
{
}

Customer::~Customer()
{

}

void Customer::print_pib()
{
	cout << pib_ << endl;
}

void Customer::print_public_info()
{
	cout << "--------------Appearance--------------------" << endl;
	this->print_appearance();
	cout << "--------------Leisure-----------------------" << endl;
	this->print_leisure();
}

void Customer::print_private_info()
{
	cout << "--------------Contact Information-----------" << endl;
	this->print_contact_info();
	cout << "--------------Passport Information----------" << endl;
	this->print_passport_info();
}

void Customer::print_appearance()
{
	cout << "Sex: " << get_sex() << endl;
	cout << "Age: " << age_ << endl;
	cout << "Height: " << height_ << endl;
	cout << "Weight: " << weight_ << endl;
}

void Customer::print_leisure()
{
	cout << "Hobby: " << hobby_ << endl;

	cout << "Bad habits: ";
	for (auto bad_habit : bad_habits_)
	{
		switch (bad_habit)
		{
		case Alcohol: cout << "alcohol,"; break;
		case Smoking: cout << "smoking,"; break;
		}
	}
	cout << endl;

	cout << "Description: " << description_ << endl;
}

void Customer::print_contact_info()
{
	cout << "Address: " << address_ << endl;
	cout << "Phone: " << phone_ << endl;
}

void Customer::print_passport_info()
{
	cout << "Birthday: " << birth_day_ << endl;

	cout << "Family state: ";
	switch (family_state_)
	{
	case Free: cout << "free"; break;
	case Married: cout << "married"; break;
	case EverythingComplicated: cout << "everything dumb"; break;
	}
	cout << endl;

	cout << "Children quantity: " << children_count_ << endl;
	cout << "Nationality code: " << nationality_code_ << endl;
	cout << "Other passport info: " << passport_data_ << endl;
}

void Customer::print_other_info()
{
	cout << "Customer code: " << customer_code_ << endl;
	cout << "Sign code: " << sign_code_ << endl;
	cout << "Relations code: " << relations_code_ << endl;
	cout << "Partner information: " << partner_info_ << endl;
}

string Customer::get_sex()
{
	switch (this->sex_)
	{
	case Male: return "male";
	case Female: return "female";
	default: return "0_o";
	}
}
